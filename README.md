# README #

Garbage Basketball VR is a game made by Jason Petterson.

### Controls ###

Requires Oculus Touch controller at the moment.

Jump - A button

Reset ball - B button

Pickup/throw - X button

Shoot gun arm - Right trigger

### Builds ###
Playable build available on my itch.io page:
https://broken-shotgun.itch.io/garbage-basketball-vr