﻿using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class Hoop : MonoBehaviour, OnTriggerListener {

    public AudioClip m_scoreFx;

    AudioSource m_audioSource;

    bool m_HasEnteredFirst = false;
    bool m_Scored = false;

    private void Start() {
        m_audioSource = GetComponent<AudioSource>();
    }

    public void TriggerEntered(Collider triggeredCollider, Collider triggeringCollider) {
        if (triggeredCollider.name == "Enter") {
            m_HasEnteredFirst = true;
        }

        if (triggeredCollider.name == "Exit" && m_HasEnteredFirst && !m_Scored) {
            m_Scored = true;
            GameManager.Instance.Score();

            m_audioSource.Stop();

            m_audioSource.clip = m_scoreFx;
            m_audioSource.Play();
        }
    }

    public void TriggerExited(Collider triggeredCollider, Collider triggeringCollider) {
        if (triggeredCollider.name == "Enter") {
            m_HasEnteredFirst = false;
            m_Scored = false;
        }
    }

}
