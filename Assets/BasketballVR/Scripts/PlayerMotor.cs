﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(PlayerManager))]
public class PlayerMotor : MonoBehaviour {

    PlayerManager m_manager;
    AudioSource m_audioSource;

    public Camera m_camera;
    public LayerMask m_pickupLayerMask;
    public float m_throwForce = 5f;
    public AudioClip m_throwSfx;
    public Shader m_glowShader;
    public Color m_glowColor = Color.yellow;

    GameObject m_heldObject;

    private GameObject m_interact;
    private Shader m_interactOrigShader;

    void Start () {
        m_manager = GetComponent<PlayerManager>();
        m_audioSource = GetComponent<AudioSource>();
	}

    public void CheckInteractGlow() {
        RaycastHit hit;
        if (Physics.SphereCast(m_manager.m_handJoint.transform.position, 0.25f, m_manager.m_handJoint.transform.forward, out hit, 10f, m_pickupLayerMask)) {
            if (m_interact != null && m_interact.name != hit.transform.gameObject.name) {
                RemoveGlow(m_interact);
                m_interact = null;
                m_interactOrigShader = null;
            }

            if (m_interact == null) {
                m_interact = hit.transform.gameObject;
                m_interactOrigShader = m_interact.GetComponent<Renderer>().material.shader;
                AddGlow(m_interact);
            }
        }
        else if (m_interact != null) {
            RemoveGlow(m_interact);
            m_interact = null;
            m_interactOrigShader = null;
        }
    }

    private void AddGlow(GameObject obj) {
        obj.GetComponent<Renderer>().material.shader = m_glowShader;
        obj.GetComponent<Renderer>().material.SetFloat("_OutlineWidth", 0.01f);
        obj.GetComponent<Renderer>().material.SetColor("_OutlineColor", m_glowColor);
    }

    private void RemoveGlow(GameObject obj) {
        obj.GetComponent<Renderer>().material.shader = m_interactOrigShader;
    }

    public void Pickup() {
        RaycastHit hit;
        //Debug.DrawRay(m_camera.transform.position, m_camera.transform.forward * 2f, Color.green, 5f);
        if (Physics.SphereCast(m_camera.transform.position, 0.25f, m_camera.transform.forward, out hit, 2f, m_pickupLayerMask)) {
            m_heldObject = hit.transform.gameObject;
            m_heldObject.transform.parent = m_manager.m_handJoint.transform;
            m_heldObject.transform.localPosition = Vector3.zero;
            m_heldObject.GetComponent<Rigidbody>().isKinematic = true;
        }
     }

    public void Throw()
    {
        if (m_heldObject != null)
        {
            m_heldObject.transform.parent = null;
            m_heldObject.GetComponent<Rigidbody>().isKinematic = false;
            m_heldObject.GetComponent<Rigidbody>().AddForce(m_camera.transform.forward * m_throwForce, ForceMode.Impulse);

            m_audioSource.Stop();
            m_audioSource.clip = m_throwSfx;
            m_audioSource.Play();

            m_heldObject = null;
        }
    }

    public void Drop()
    {
        if (m_heldObject != null)
        {
            m_heldObject.transform.parent = null;
            m_heldObject.GetComponent<Rigidbody>().isKinematic = false;
            m_heldObject = null;
        }
    }

    public bool IsHoldingObject() {
        return m_heldObject != null;
    }
}
