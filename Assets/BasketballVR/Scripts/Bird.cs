﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bird : MonoBehaviour {

    public GameObject m_explosionPrefab;
    public GameObject[] m_waypoints;

    private Rigidbody m_rigidbody;
    private AudioSource m_audioSource;

    private Transform m_currentTarget;
    private int m_currentId;
    private float m_speed = 5f;

	void Start () {
        m_rigidbody = GetComponent<Rigidbody>();
        m_audioSource = GetComponent<AudioSource>();

        m_currentTarget = m_waypoints[0].transform;
	}

    private void FixedUpdate() {
        FollowPath();
    }

    void FollowPath() {
        Vector3 relativePos = m_currentTarget.position - transform.position;
        Quaternion rotation = Quaternion.LookRotation(relativePos * Time.deltaTime);

        m_rigidbody.MoveRotation(rotation);
        m_rigidbody.MovePosition(Vector3.MoveTowards(transform.position, m_currentTarget.position, m_speed * Time.deltaTime));

        if (Vector3.Distance(transform.position, m_currentTarget.position) < 0.1f) {
            m_currentId++;

            if (m_currentId >= m_waypoints.Length) {
                m_currentId = 0;
            }

            m_currentTarget = m_waypoints[m_currentId].transform;
        }
    }

    private void OnCollisionEnter(Collision collision) {
        if (collision.gameObject.tag == "Bullet") {
            SpawnExplosion();
        }
    }

    private void SpawnExplosion() {
        GameObject explosion = Instantiate(m_explosionPrefab, transform.position, Quaternion.identity) as GameObject;
        Destroy(explosion, 3f);

        if (!m_audioSource.isPlaying)
            m_audioSource.Play();
    }
}
