﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

    public static GameManager Instance { get; private set; }

    public GameObject m_ballPrefab;
    public GameObject m_frisbeePrefab;

    public Text m_scoreText;

    int m_score = 0;
    Vector3 m_ballStartPosition;
    Vector3 m_frisbeeStartPosition;

    [HideInInspector]
    public GameObject m_ball;

    [HideInInspector]
    public GameObject m_frisbee;

    bool m_IsInitialReset = true;

	void Awake () {
		if (Instance == null) {
            Instance = this;
        }
        else {
            Destroy(gameObject);
        }

        ResetGame();
	}

    public void ResetGame() {
        m_score = 0;
        ResetBall();
        ResetFrisbee();

        m_IsInitialReset = false;
    }

    public void ResetBall() {
        m_ball = GameObject.FindGameObjectWithTag("Ball");

        // set ball start position on initial reset
        if (m_IsInitialReset && m_ball != null) {
            m_ballStartPosition = m_ball.transform.position;
        }

        if (m_ball == null) {
            m_ball = Instantiate(m_ballPrefab, m_ballStartPosition, Quaternion.identity) as GameObject;
        }
        else if (m_ball.transform.parent == null) { // make sure not being held
            m_ball.transform.position = m_ballStartPosition;
            m_ball.GetComponent<Rigidbody>().velocity = Vector3.zero;
            m_ball.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
        }
    }

    public void ResetFrisbee() {
        m_frisbee = GameObject.FindGameObjectWithTag("Disc");

        // set ball start position on initial reset
        if (m_IsInitialReset && m_frisbee != null) {
            m_frisbeeStartPosition = m_frisbee.transform.position;
        }

        if (m_frisbee == null) {
            m_frisbee = Instantiate(m_frisbeePrefab, m_frisbeeStartPosition, Quaternion.identity) as GameObject;
        }
        else if (m_frisbee.transform.parent == null) { // make sure not being held
            m_frisbee.transform.position = m_frisbeeStartPosition;
            m_frisbee.GetComponent<Rigidbody>().velocity = Vector3.zero;
            m_frisbee.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
        }
    }

    public void Score() {
        m_score++;
        m_scoreText.text = "SCORE : " + m_score.ToString();
        Invoke("ResetBall", 3f);
    }
	
}
