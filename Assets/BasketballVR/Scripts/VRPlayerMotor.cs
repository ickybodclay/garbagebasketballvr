﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(OVRPlayerController))]
[RequireComponent(typeof(VRPlayerManager))]
[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(AudioSource))]
public class VRPlayerMotor : MonoBehaviour {
    OVRPlayerController m_controller;
    VRPlayerManager m_manager;
    AudioSource m_audioSource;

    public Transform m_CenterEyeAnchor;
    public LayerMask m_pickupLayerMask;
    public float m_throwForce = 5f;
    public AudioClip m_throwSfx;

    GameObject m_ball;

    void Start()
    {
        m_controller = GetComponent<OVRPlayerController>();
        m_manager = GetComponent<VRPlayerManager>();
        m_audioSource = GetComponent<AudioSource>();
    }

    public void Pickup()
    {
        RaycastHit hit;
        //Debug.DrawRay(m_camera.transform.position, m_camera.transform.forward * 10f, Color.green, 5f);
        if (Physics.SphereCast(m_CenterEyeAnchor.position, 0.25f, m_CenterEyeAnchor.forward, out hit, 10f, m_pickupLayerMask))
        {
            m_ball = hit.transform.gameObject;
            m_ball.transform.parent = m_manager.m_handJoint.transform;
            m_ball.transform.localPosition = Vector3.zero;
            m_ball.transform.localRotation = Quaternion.identity;
            m_ball.transform.localScale = new Vector3(5f, 5f, 0.5f); // hack to undo the scaling of the arm, ideally we'd just have the arm model be the proper size and of scale 1,1,1
            m_ball.GetComponent<Rigidbody>().isKinematic = true;
        }
    }

    public void Throw()
    {
        if (m_ball != null)
        {
            m_ball.transform.parent = null;
            m_ball.GetComponent<Rigidbody>().isKinematic = false;
            m_ball.GetComponent<Rigidbody>().AddForce(m_CenterEyeAnchor.forward * m_throwForce, ForceMode.Impulse);

            m_audioSource.Stop();
            m_audioSource.clip = m_throwSfx;
            m_audioSource.Play();

            m_ball = null;
        }
    }

    public void Drop()
    {
        if (m_ball != null)
        {
            m_ball.transform.parent = null;
            m_ball.GetComponent<Rigidbody>().isKinematic = false;
            m_ball = null;
        }
    }

    public void Jump()
    {
        m_controller.Jump();
    }

    public bool IsHoldingBall()
    {
        return m_ball != null;
    }
}
