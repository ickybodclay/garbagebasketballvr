﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class Basket : MonoBehaviour {
    public AudioClip m_successFx;

    AudioSource m_audioSource;

    public void Start()
    {
        m_audioSource = GetComponent<AudioSource>();
    }

    private void OnTriggerEnter(Collider other) {
        if (other.tag == "Ball") {
            m_audioSource.Stop();

            m_audioSource.clip = m_successFx;
            m_audioSource.Play();

            GameManager.Instance.Score();

            Destroy(other.gameObject);
        }
    }
}
