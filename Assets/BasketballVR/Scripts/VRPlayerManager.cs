﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VRPlayerManager : MonoBehaviour {
    VRPlayerMotor m_motor;

    public GameObject m_handJoint;

    void Start()
    {
        m_motor = GetComponent<VRPlayerMotor>();
    }

    void Update()
    {
        HandleInput();
    }

    void HandleInput()
    {
        
        if (m_motor.IsHoldingBall() && Input.GetButtonDown("Fire3"))
        {
            m_motor.Throw();
        }
        else if (Input.GetButtonDown("Fire3"))
        {
            m_motor.Pickup();
        }

        if (Input.GetButtonDown("Fire1"))
        {
            m_motor.Jump();
        }

        if (Input.GetButtonDown("Fire2"))
        {
            GameManager.Instance.ResetBall();
        }
    }
}
