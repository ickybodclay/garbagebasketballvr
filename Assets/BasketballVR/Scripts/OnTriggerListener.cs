﻿using UnityEngine;

public interface OnTriggerListener {
    void TriggerEntered(Collider triggeredCollider, Collider triggeringCollider);
    void TriggerExited(Collider triggeredCollider, Collider triggeringCollider);
}
