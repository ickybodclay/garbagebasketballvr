﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : MonoBehaviour {

    PlayerMotor m_motor;

    public GameObject m_handJoint;
    public Gun m_gun;

	void Start () {
        m_motor = GetComponent<PlayerMotor>();

        StartCoroutine(CheckInteractGlow());
    }
	
	void Update () {
		if (Input.GetKeyDown(KeyCode.E)) {
            m_motor.Pickup();
        }
        else if (!Input.GetKey(KeyCode.E)) {
            m_motor.Drop();
        }

        if (Input.GetMouseButton(0)) {
            m_gun.Shoot();
        }

        if (m_motor.IsHoldingObject() && Input.GetMouseButtonDown(1)) {
            m_motor.Throw();
        }

        if (Input.GetKeyDown(KeyCode.R)) {
            GameManager.Instance.ResetBall();
        }
	}

    IEnumerator CheckInteractGlow() {
        while (isActiveAndEnabled) {
            m_motor.CheckInteractGlow();
            yield return new WaitForSeconds(0.1f);
        }
    }
}
