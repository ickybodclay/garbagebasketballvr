﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : MonoBehaviour {
    public GameObject m_BulletPrefab;
    public float m_bulletSpeed = 10f;

    private bool m_canShoot = true;
    private float m_cooldown = 0.1f;

    private AudioSource m_AudioSoruce;

    private void Start() {
        m_AudioSoruce = GetComponent<AudioSource>();
    }

    public void Shoot() {
        if (!m_canShoot) return;

        GameObject bullet = Instantiate(m_BulletPrefab, transform.position, Quaternion.identity) as GameObject;
        bullet.GetComponent<Rigidbody>().AddForce(transform.forward * m_bulletSpeed, ForceMode.Impulse);
        Destroy(bullet, 5f);

        m_AudioSoruce.pitch = Random.Range(0.95f, 1.05f);
        m_AudioSoruce.Play();

        m_canShoot = false;
        Invoke("resetCooldown", m_cooldown);
    }

    private void resetCooldown() {
        m_canShoot = true;
    }
}
