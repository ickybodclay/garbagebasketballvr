﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VRTouchPlayerManager : MonoBehaviour {
    VRTouchPlayerMotor m_motor;

    public Gun m_gun;
    public GameObject m_handJoint;

    void Start() {
        m_motor = GetComponent<VRTouchPlayerMotor>();

        StartCoroutine(CheckInteractGlow());
    }

    void Update() {
        HandleInput();
    }

    void HandleInput() {

        if (m_motor.IsHoldingObject() && Input.GetButtonDown("Fire3")) {
            m_motor.Throw();
        }
        else if (Input.GetButtonDown("Fire3")) {
            m_motor.Pickup();
        }

        if (Input.GetButtonDown("Fire1")) {
            m_motor.Jump();
        }

        if (Input.GetButtonDown("Fire2")) {
            GameManager.Instance.ResetBall();
            GameManager.Instance.ResetFrisbee();
        }

        if (OVRInput.Get(OVRInput.Axis1D.PrimaryIndexTrigger, OVRInput.Controller.RTouch) != 0f) {
            m_gun.Shoot();
        }
    }

    IEnumerator CheckInteractGlow() {
        while (isActiveAndEnabled) {
            m_motor.CheckInteractGlow();
            yield return new WaitForSeconds(0.1f);
        }
    }
}
