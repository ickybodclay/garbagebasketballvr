﻿using UnityEngine;

public class OnTriggerMonitor : MonoBehaviour {

    [SerializeField]
    MonoBehaviour m_Listener;

    OnTriggerListener m_TriggerListener;

    void Start() {
        if (m_Listener is OnTriggerListener) {
            m_TriggerListener = m_Listener as OnTriggerListener;
        }
    }

    void OnTriggerEnter(Collider other) {
        if (m_TriggerListener != null) {
            m_TriggerListener.TriggerEntered(this.GetComponent<Collider>(), other);
        }
    }

    void OnTriggerExit(Collider other) {
        if (m_TriggerListener != null) {
            m_TriggerListener.TriggerExited(this.GetComponent<Collider>(), other);
        }
    }
}
